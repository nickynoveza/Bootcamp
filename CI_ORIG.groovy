// Constants
def SGCJavaBuildPipelineFolderName= "/${PROJECT_NAME}"
def (workspacename, projectname) = "${PROJECT_NAME}".tokenize('/')

// Jobs
def generateBuildPipelineView = buildPipelineView(SGCJavaBuildPipelineFolderName + "/Java_Build_Pipeline_View")
def generateBuildJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Build_Java_Project")
def generateAppScanCWEJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/AppScan_Generate_CWE_Report")
def generateUpdateJiraAppScanCWEJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_AppScan_CWE_Result")
def generateAppScanOWASPJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/AppScan_Generate_OWASP_Report")
def generateUpdateJiraAppScanOWASPJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_AppScan_OWASP_Result")
def generateSonarJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Sonar_Java_Project")
def generateSaveLatestDevelopmentCIJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Save_Latest_Development_CI")
def generateRevertPreviousCommitJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Revert_Previous_Tag")
def generateUpdateJiraIssueInProgressJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_Issue_In_Progress")
def generateUpdateJiraIssueAwaitingDeploymentJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_Issue_Awaiting_Deployment")
def generateUpdateJiraIssueCodeReviewJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_Issue_Code_Review")	
def generateUpdateJiraCommentCIStatus = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Update_Jira_Comment_CIStatus")
def generateValidateGitPushJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Validate_Git_Push")
def generateCreateBranchJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Create_Feature_Branch")
def generateDeleteBranchJob = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Delete_Feature_Branch")
def generateSaveTag = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Save_Deployment_Tag")
def generateSendEmail = freeStyleJob(SGCJavaBuildPipelineFolderName + "/Send_Email")
def generateCiUploadNexus = freeStyleJob(SGCJavaBuildPipelineFolderName + "/CI_Upload_Package_to_Nexus")
// Setup generateBuildPipelineJobs
// ##### GENERATE BUILD PIPELINE VIEW #####

generateBuildPipelineView.with {
	title('Java_Build_Pipeline_View')
    displayedBuilds(5)
    selectedJob(SGCJavaBuildPipelineFolderName + "/Build_Java_Project")
    alwaysAllowManualTrigger()
    showPipelineParameters()
    refreshFrequency(5)
}	
// ##### END OF BUILD PIPELINE VIEW #####
// ##### VALIDATE GIT PUSH JOB #####
generateValidateGitPushJob.with {
	logRotator {
		daysToKeep(7)
		artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    configure { Node project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob{
           				 }
           			 }
      		  }
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            'rebuildDisabled' false
		}
	}
	label('linux2')
	scm {
			git {
					remote {
							url("http://10.0.66.104:9080/${PROJECT_NAME}.git")
							credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
					}
					branch('**')
			}
	}
	configure { project ->
				project / 'triggers' / 'com.dabsquared.gitlabjenkins.GitLabPushTrigger' {
					spec('')
					triggerOnPush('true')
					triggerOnMergeRequest('false')
					triggerOpenMergeRequestOnPush('never')
					ciSkip('true') 	
					setBuildDescription('true')
					addNoteOnMergeRequest('true')
					addVoteOnMergeRequest('true')
					addCiMessage('false')
					addVoteOnMergeRequest('true')
					branchFilterType('All')
					includeBranchesSpec('')
					excludeBranchesSpec('')
					targetBranchRegex('')
					acceptMergeRequestOnSuccess('false')
				}
	}
	wrappers {
		preBuildCleanup()
        timestamps()
    }
	publishers {
		flexiblePublish {
			conditionalAction{
				condition {
					not {
						stringsMatch('$gitlabUserName','jenkins',false)
					}
				}
				publishers{
					downstreamParameterized {
						trigger('Build_Java_Project'){
							condition('ALWAYS')
							parameters {
								predefinedProps([gitlabActionType:'$gitlabActionType', gitlabBranch:'$gitlabBranch', gitlabSourceBranch:'$gitlabSourceBranch', gitlabSourceRepoHomepage:'$gitlabSourceRepoHomepage', gitlabSourceRepoHttpUrl:'$gitlabSourceRepoHttpUrl', gitlabSourceRepoName:'$gitlabSourceRepoName', gitlabSourceRepoSshUrl:'$gitlabSourceRepoSshUrl', gitlabSourceRepoURL:'$gitlabSourceRepoURL', gitlabTargetBranch:'$gitlabTargetBranch', gitlabUserEmail:'$gitlabUserEmail', gitlabUserName:'$gitlabUserName'])
							}
						}

					}
				}
			}
		}
	}
	configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1596')
					}
			}
}
// ##### END OF VALIDATE GIT PUSH JOB #####
// ##### GENERATE BUILD JOB #####
generateBuildJob.with {
	logRotator {
		daysToKeep(7)
		artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
	configure { Node project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob{
           				 }
           			 }
      		  }
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            'rebuildDisabled' false
		}
	}
	label('linux2')
	scm {
			git {
					remote {
							url("http://10.0.66.104:9080/${PROJECT_NAME}.git")
							credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
					}
					branch('refs/heads/$gitlabBranch')
			}
	}
	wrappers {
		preBuildCleanup()
        timestamps()
    }
	configure { Node project ->
        project / 'buildWrappers' / 'EnvInjectPasswordWrapper' (plugin:'envinject@1.91.3') { 
             injectGlobalPasswords true 
             maskPasswordParameters true
             passwordEntries ('')
             }
}
	steps {
		environmentVariables {
			propertiesFile('${JENKINS_HOME}/cicd_config/Common/entrypoint.txt')
		}
	configure { project ->
       project / 'builders' / 'hudson.tasks.Maven' / 'settings' (class:'jenkins.mvn.DefaultSettingsProvider') { 
     		  }
   		 }
 	   configure { project ->
       project / 'builders' / 'hudson.tasks.Maven' / 'globalSettings' (class:'jenkins.mvn.DefaultGlobalSettingsProvider') {
       }
 }
			maven {
				mavenInstallation('ADOP Maven')
				goals('''
clean
org.jacoco:jacoco-maven-plugin:prepare-agent
-Dmaven.test.failure.ignore=true
package
				''')
			}
			conditionalSteps {
				condition {
					stringsMatch('$gitlabSourceBranch', 'development', false)
				}
				runner('Fail')
				steps {
					steps {
						shell('''
ISSUE_KEY=$(git log --merges | grep "Merge branch" | head -1 | grep -Po NTP-[0-9]+)
echo "ISSUE_KEY=$ISSUE_KEY" > properties.txt

FEATURE_BRANCH=$(git log --merges | grep "Merge branch" | head -1 | grep -Po FEATURE-NTP-[0-9]+)
echo "FEATURE_BRANCH=$FEATURE_BRANCH" >> properties.txt

#LAST_MERGE_COMMIT=$(git log --merges --first-parent | grep "^commit" | head -1 | cut -d' ' -f2)
#echo "LAST_MERGE_COMMIT=$LAST_MERGE_COMMIT" >> properties.txt

#REVERT_MERGE_FLAG=$(git log | head -1 | cut -d' ' -f2 | git show . | awk '/Revert \"Merge branch/{print $0}' | wc -l)
#echo "REVERT_MERGE_FLAG=$REVERT_MERGE_FLAG" >> properties.txt			
						''')	
					}
				}
           }
	}
	configure { project ->
			project / 'builders' / 'hudson.plugins.parameterizedtrigger.TriggerBuilder' / 'configs' / 'hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig'{
					projects('Save_Latest_Development_CI')
					condition('ALWAYS')
					triggerWithNoParameters('false')
					buildAllNodesWithLabel('false')
			}
	}
	configure { project ->
			project / 'builders' / 'hudson.plugins.parameterizedtrigger.TriggerBuilder' / 'configs' / 'hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig' / 'configs' / 'hudson.plugins.parameterizedtrigger.PredefinedBuildParameters' {
					properties('LATEST_DEV_CI_URL=$BUILD_URL')
			}
	}
	configure { project ->
			project / 'publishers' / 'hudson.tasks.junit.JUnitResultArchiver' {
					testResults('**/target/surefire-reports/*.xml')
					keepLongStdio('false')
					healthScaleFactor('1.0')
		}
	}
	configure { project ->
			project / 'publishers' / 'com.dabsquared.gitlabjenkins.publisher.GitLabCommitStatusPublisher' {}
	}
	publishers {
		archiveArtifacts {
			pattern('**/*')
		}
		downstreamParameterized {
			trigger('Sonar_Java_Project') {
				condition('UNSTABLE_OR_BETTER')
				parameters {
					predefinedProps([CUSTOM_WORKSPACE: '$WORKSPACE', GITLAB_SOURCE_BRANCH: '$gitlabSourceBranch',GITLAB_SOURCE_REPOHOMEPAGE:'$gitlabSourceRepoHomepage',GITLAB_USERNAME:'$GITLAB_USERNAME',GITLAB_PASSWORD:'$GITLAB_PASSWORD',GITLAB_URL:'$GITLAB_URL',SONAR_QUALITY_GATE:'$SONAR_QUALITY_GATE',AppScanWS:'http://10.0.66.104:9080/'+workspacename+'/'+projectname+'.git'])
					propertiesFile('properties.txt', false)
					currentBuild()
				}
			}
		}
    }
    publishers {
    	downstreamParameterized {
    		trigger('Update_Jira_Comment_CIStatus'){
    			condition('FAILED')
    			parameters{
    				predefinedProps([JIRA_COMMENT:'CI Build Failed',JENKINS_BUILD_URL:'$BUILD_URL',JIRA_PROJECT:'NTP',GITLAB_SOURCE_BRANCH:'$gitlabSourceBranch'])
    			}
    		}
    	}
    	downstreamParameterized {
    		    trigger('Revert_Previous_Tag') {
    			condition('FAILED')
    			parameters {
    				propertiesFile('properties.txt', failTriggerOnMissing = true)
    			}
    		}
    	}
		downstreamParameterized {
    		    trigger('Send_Email') {
    			condition('FAILED')
    			parameters {
				predefinedProps([BUILDURL:'$BUILD_URL',JOBNAME:'$JOB_NAME',BUILDNUMBER:'$BUILD_NUMBER'])
    			}
    		}
    	}
    }
	 configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1583')
					}
			}
}

// ##### END OF BUILD JOB 
// ##### GENERATE APPSCAN CWE JOB #####

generateAppScanCWEJob.with {
	    logRotator {
		daysToKeep(7)
		artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
configure { Node project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob{
           				 }
           			 }
      		  }
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            'rebuildDisabled' false
		}
	}
	label('appscan')
scm {
		git {
			remote {
				url('${AppScanWS}')
				credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
			}
			branch('*/$gitlabSourceBranch')
		}
	}
configure { project ->
		project / 'scm' / 'userRemoteConfigs' {
            'hudson.plugins.git.UserRemoteConfig' {
				url ('${AppScanWS}')
                credentialsId ('05443b60-8117-43cb-9f49-34a50cc64a30')
					}
				}
			}
configure { project ->
		project / 'scm' / 'doGenerateSubmoduleConfigurations' (false) {
				}
			}
configure { project ->
		project / 'scm' / 'submoduleCfg' (class:"list") {
				}
			}

	wrappers {
		preBuildCleanup()
        timestamps()
		environmentVariables {
			propertiesFile('C:\\Jenkins\\entrypoint.txt')
			groovy('''
TimeZone.setDefault(TimeZone.getTimeZone('UTC'))
def now = new Date()
def map = [BUILD_ID_LONG: now.format("yyyyMMdd_HHmm")]
return map
''')
		}
    }
configure { Node project ->
        project / 'buildWrappers' / 'EnvInjectPasswordWrapper' (plugin:'envinject@1.91.3') { 
             injectGlobalPasswords true 
             maskPasswordParameters true
             passwordEntries ('')
             }
	}
	steps {
		batchFile('''
set APPSCAN_HOME=C:\\Program Files (x86)\\IBM\\AppScanSource\\bin
set JAVA_HOME=C:\\Program Files\\Java\\jdk1.8.0_111
set APACHE_HOME=C:\\Program Files\\apache-maven-3.3.9\\bin
set PATH=%APPSCAN_HOME%;%APACHE_HOME%;%JAVA_HOME%
set TMP=C:\\Temp
set TEMP=C:\\Temp

mvn -Dmaven.test.skip=true clean package ounce:application ounce:project -Dounce.wait=true ounce:report -Dounce.assessmentOutput="%gitlabBranch%_%gitlabSourceRepoName%.ozasmt" -Dounce.reportType="CWE SANS Top 25 2011" -Dounce.reportOutputType="pdf-annotated" -Dounce.includeSrcBefore=5 -Dounce.includeSrcAfter=5 -Dounce.reportOutputPath="%WORKSPACE%\\%gitlabBranch%_%gitlabSourceRepoName%_CWE SANS Top 25 2011_%BUILD_ID_LONG%.pdf"
''')
		batchFile('''
if NOT "%gitlabBranch%" == "development" (set ISSUE_KEY=%gitlabBranch:~8%) ELSE (set ISSUE_KEY=%FEATURE_BRANCH:~8%)
echo %ISSUE_KEY%

C:\\Jenkins\\curl\\src\\curl.exe -f -D- -k -u %JIRA_USERNAME%:%JIRA_PASSWORD% -X POST -H "X-Atlassian-Token: nocheck" -F file=@"%WORKSPACE%\\%gitlabBranch%_%gitlabSourceRepoName%_CWE SANS Top 25 2011_%BUILD_ID_LONG%.pdf" %JIRA_URL%/rest/api/2/issue/%ISSUE_KEY%/attachments
''')
	}
	publishers {
		archiveArtifacts {
			pattern('*.ozasmt')
			pattern('*.pdf')
		}
		downstreamParameterized {
			trigger('AppScan_Generate_OWASP_Report') {
				condition('UNSTABLE_OR_BETTER')
				parameters {
					currentBuild()
				}
			}
		}
		downstreamParameterized {
			trigger('Update_Jira_AppScan_CWE_Result') {
				condition('UNSTABLE_OR_BETTER')
				parameters {
					currentBuild()
					predefinedProps([JIRA_COMMENT:'AppScan CWE Successful. New Report is now uploaded.',JENKINS_BUILD_URL:'$BUILD_URL'])
				}
			}
		}
		downstreamParameterized {
			trigger('Update_Jira_AppScan_CWE_Result') {
				condition('FAILED')
				parameters {
					currentBuild()
					predefinedProps([JIRA_COMMENT:'AppScan CWE Failed. Please see build console output.',JENKINS_BUILD_URL:'$BUILD_URL'])
				}
			}
		}
		downstreamParameterized {
    	    trigger('Send_Email') {
				condition('FAILED')
				parameters {
					predefinedProps([BUILDURL:'$BUILD_URL',JOBNAME:'$JOB_NAME',BUILDNUMBER:'$BUILD_NUMBER'])
				}
    		}
    	}
        configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/2207')
					}
			}
		wsCleanup()
    }
}

// ##### END OF APPSCAN CWE JOB #####
// ##### GENERATE UPDATE JIRA APPSCAN CWE RESULT JOB #####

generateUpdateJiraAppScanCWEJob.with {
	logRotator {
		daysToKeep(7)
		artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
label('linux2')
	steps{
		shell('''
if [ "$gitlabSourceBranch" != "development" ]; then
  JIRA_TICKET=$(echo "$gitlabSourceBranch" | sed "s/FEATURE-//g")
  echo "JIRA_TICKET=$JIRA_TICKET" > properties.txt
else
  echo "Not a feature branch, no action to take."
  exit 0
fi

echo "GITLAB_SOURCE_BRANCH=$gitlabSourceBranch" >> properties.txt
			''')
		environmentVariables{
			propertiesFile('properties.txt')
		}
        conditionalSteps {
            condition {
                    not {
                        stringsMatch('$GITLAB_SOURCE_BRANCH', 'development', false)
                    }
                }
			 runner('Fail')
			 steps{}
        }
	}
	configure { project ->
		project / 'builders' / 'org.jenkinsci.plugins.conditionalbuildstep.ConditionalBuilder' / 'conditionalbuilders' / 'info.bluefloyd.jenkins.IssueUpdatesBuilder' {
			'restAPIUrl'('https://10.0.66.78:8443/jira/rest/api/2')
			'userName'('jenkins')
			'password'('scdsdevjenkinsadmin01')
			'jql'('project=NTP AND issuekey=$ISSUE_KEY')
			'workflowActionName'('')
			'comment'('$JIRA_COMMENT - $JENKINS_BUILD_URL')
			'customFieldId'('')
			'customFieldValue'('')
			'resettingFixedVersions'('false')
			'createNonExistingFixedVersions'('false')
			'failIfJqlFails'('false')
			'failIfNoIssuesReturned'('false')
			'failIfNoJiraConnection'('false')
		}
	}
configure { project ->
		project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
			postURL ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/2209')
            		}
			}    
 
}

// ##### END OF UPDATE JIRA APPSCAN CWE RESULT JOB #####
// ##### GENERATE APPSCAN OWASP JOB #####

generateAppScanOWASPJob.with {
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
	label('appscan')
	scm {
		git {
			remote {
				url('${AppScanWS}')
				credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
			}
			branch('*/$gitlabSourceBranch')
		}
	}
    configure { project ->
		project / 'scm' / 'submoduleCfg' (class:"list") {
            
				}
			}
	wrappers {
		preBuildCleanup()
        timestamps()
		environmentVariables {
			propertiesFile('C:\\Jenkins\\entrypoint.txt')
			groovy('''
TimeZone.setDefault(TimeZone.getTimeZone('UTC'))
def now = new Date()
def map = [BUILD_ID_LONG: now.format("yyyyMMdd_HHmm")]
return map
''')
		}
    }
configure { Node project ->
        project / 'buildWrappers' / 'EnvInjectPasswordWrapper' (plugin:'envinject@1.91.3') { 
             injectGlobalPasswords true 
             maskPasswordParameters true
             passwordEntries ('')
             }
	}
	 steps {
		copyArtifacts('AppScan_Generate_CWE_Report') {
            includePatterns('*.ozasmt')
            buildSelector {
                upstreamBuild (false)
             }  
            fingerprintArtifacts(true)
         }   
		batchFile('''
set APPSCAN_HOME=C:\\Program Files (x86)\\IBM\\AppScanSource\\bin
set JAVA_HOME=C:\\Program Files\\Java\\jdk1.8.0_111
set APACHE_HOME=C:\\Program Files\\apache-maven-3.3.9\\bin
set PATH=%APPSCAN_HOME%;%APACHE_HOME%;%JAVA_HOME%
set TMP=C:\\Temp
set TEMP=C:\\Temp

mvn -Dmaven.test.skip=true package ounce:application ounce:project -Dounce.wait=true ounce:report -Dounce.existingAssessmentFile="%gitlabBranch%_%gitlabSourceRepoName%.ozasmt" -Dounce.reportType="OWASP Top 10 2013" -Dounce.reportOutputType="pdf-annotated" -Dounce.includeSrcBefore=5 -Dounce.includeSrcAfter=5 -Dounce.reportOutputPath="%WORKSPACE%\\%gitlabBranch%_%gitlabSourceRepoName%_OWASP Top 10 2013_%BUILD_ID_LONG%.pdf"
''')
		batchFile('''
if NOT "%gitlabBranch%" == "development" (set ISSUE_KEY=%gitlabBranch:~8%) ELSE (set ISSUE_KEY=%FEATURE_BRANCH:~8%)
echo %ISSUE_KEY%

C:\\Jenkins\\curl\\src\\curl.exe -f -D- -k -u %JIRA_USERNAME%:%JIRA_PASSWORD% -X POST -H "X-Atlassian-Token: nocheck" -F file=@"%WORKSPACE%\\%gitlabBranch%_%gitlabSourceRepoName%_OWASP Top 10 2013_%BUILD_ID_LONG%.pdf" %JIRA_URL%/rest/api/2/issue/%ISSUE_KEY%/attachments
''')
}
	publishers {
		archiveArtifacts {
			pattern('*.pdf')
		}
		downstreamParameterized {
			trigger('Update_Jira_AppScan_OWASP_Result') {
				condition('UNSTABLE_OR_BETTER')
				parameters {
					currentBuild()
					predefinedProps([JIRA_COMMENT:'AppScan OWASP Successful. New Report is now uploaded.',JENKINS_BUILD_URL:'$BUILD_URL'])
				}
			}
		}
		downstreamParameterized {
			trigger('Update_Jira_AppScan_OWASP_Result') {
				condition('FAILED')
				parameters {
					currentBuild()
					predefinedProps([JIRA_COMMENT:'AppScan OWASP Failed. Please see build console output.',JENKINS_BUILD_URL:'$BUILD_URL'])
				}
			}
		}
		flexiblePublish {
			conditionalAction {
				condition {
					and {
						stringsMatch('$GITLAB_SOURCE_BRANCH', 'R0', false)						
									}
								}
						}	
				}
				publishers {
					downstreamParameterized {
						trigger('Save_Deployment_Tag') {
							condition('SUCCESS')
							parameters {
								currentBuild()
							}
						}
					}
					downstreamParameterized {
						trigger('Revert_Previous_Tag') {
							condition('FAILED')
							parameters {
								predefinedProps([ISSUE_KEY: '$ISSUE_KEY'])
							}
						}
					}
					downstreamParameterized {
						trigger('Update_Jira_Issue_Awaiting_Deployment,') {
							condition('SUCCESS')
							parameters {
								currentBuild()
							}
						}
					}
				}
			}
		}
		downstreamParameterized {
    	    trigger('Send_Email') {
				condition('FAILED')
    			parameters {
					predefinedProps([BUILDURL:'$BUILD_URL',JOBNAME:'$JOB_NAME',BUILDNUMBER:'$BUILD_NUMBER'])
    			}
    		}
    	}
configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/2208')
					}
			}
		wsCleanup()
    }
}

// ##### END OF APPSCAN OWASP JOB #####
// ##### GENERATE UPDATE JIRA APPSCAN OWASP RESULT JOB #####

generateUpdateJiraAppScanOWASPJob.with {
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
label('linux2')
	steps{
		shell('''
if [ "$gitlabSourceBranch" != "development" ]; then
  JIRA_TICKET=$(echo "$gitlabSourceBranch" | sed "s/FEATURE-//g")
  echo "JIRA_TICKET=$JIRA_TICKET" > properties.txt
else
  echo "Not a feature branch, no action to take."
  exit 0
fi

echo "GITLAB_SOURCE_BRANCH=$gitlabSourceBranch" >> properties.txt
			''')
		environmentVariables{
			propertiesFile('properties.txt')
		}
        conditionalSteps {
            condition {
                    not {
                        stringsMatch('$GITLAB_SOURCE_BRANCH', 'development', false)
                    }
                }
			 runner('Fail')
			 steps{}
        }
	}
	configure { project ->
		project / 'builders' / 'org.jenkinsci.plugins.conditionalbuildstep.ConditionalBuilder' / 'conditionalbuilders' / 'info.bluefloyd.jenkins.IssueUpdatesBuilder' {
			'restAPIUrl'('https://10.0.66.78:8443/jira/rest/api/2')
			'userName'('jenkins')
			'password'('scdsdevjenkinsadmin01')
			'jql'('project=NTP AND issuekey=$ISSUE_KEY')
			'workflowActionName'('')
			'comment'('$JIRA_COMMENT - $JENKINS_BUILD_URL')
			'customFieldId'('')
			'customFieldValue'('')
			'resettingFixedVersions'('false')
			'createNonExistingFixedVersions'('false')
			'failIfJqlFails'('false')
			'failIfNoIssuesReturned'('false')
			'failIfNoJiraConnection'('false')
		}
	}
    configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/2210')
					}
			}
}

// ##### END OF UPDATE JIRA APPSCAN OWASP RESULT JOB #####
// ##### GENERATE SONAR JOB #####

generateSonarJob.with {
logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
	label('linux2')
		wrappers {
		preBuildCleanup()
        timestamps()
    }
    steps {
		copyArtifacts('Build_Java_Project') {
            includePatterns('**/*')
            buildSelector {
                upstreamBuild(false)
            }
        }
		shell('''#!/bin/bash
echo "Check for Sonar Exclusion"
find . -name pom.xml > pom_list.txt

while read line
do
	echo "Checking $line"
	if grep -q "sonar.exclusions" "$line"; then \\
		echo "Sonar Exclusions found in $line"
		checker=1
	fi

	if grep -q "sonar.coverage.exclusions" "$line"; then \\
		echo "Sonar Coverage Exclusions found in $line"
		checker=1
	fi

	if grep -q "sonar.test.exclusions" "$line"; then \\
		echo "Sonar Test Exclusions found in $line"
		checker=1
	fi
	
done<pom_list.txt

if [[ $checker == 1 ]]; then \\
	echo "Sonar Exclusions found. Please check the affected pom file/s."
	echo "End check for Sonar Exclusion"
	exit 1
fi

echo "End check for Sonar Exclusion"''')
    	maven {
    		goals('''
$SONAR_MAVEN_GOAL
-Dsonar.branch=$GITLAB_SOURCE_BRANCH
-Dsonar.host.url=$SONAR_HOST_URL
-Dsonar.qualitygate=$SONAR_QUALITY_GATE
-Dsonar.projectName='''+"${PROJECT_NAME}"+'''
    			''')
    		mavenInstallation('ADOP Maven')
    	}
    }
 configure { project ->
       project / 'builders' / 'hudson.tasks.Maven' / 'settings' (class:'jenkins.mvn.DefaultSettingsProvider') { 
       }
    }
 configure { project ->
       project / 'builders' / 'hudson.tasks.Maven' / 'globalSettings' (class:'jenkins.mvn.DefaultGlobalSettingsProvider') {
       }
 }
	publishers {
		downstreamParameterized {
			trigger('Update_Jira_Comment_CIStatus') {
				condition('SUCCESS')
				parameters {
					predefinedProps([JIRA_COMMENT: 'Sonar Analysis Passed', JENKINS_BUILD_URL: '$BUILD_URL', JIRA_PROJECT: 'NTP', GITLAB_SOURCE_BRANCH: '$GITLAB_SOURCE_BRANCH'])
				}
			}
		}
		downstreamParameterized {
			trigger('Update_Jira_Comment_CIStatus') {
				condition('FAILED')
				parameters {
					predefinedProps([JIRA_COMMENT: 'Sonar Analysis Failed', JENKINS_BUILD_URL: '$BUILD_URL', JIRA_PROJECT: 'NTP', GITLAB_SOURCE_BRANCH: '$GITLAB_SOURCE_BRANCH'])
				}
			}
		}
		downstreamParameterized {
				trigger('CI_Upload_Package_to_Nexus') {
				condition('UNSTABLE_OR_BETTER')
				parameters {
					currentBuild()
				}
			}
		}
        downstreamParameterized {
				trigger('AppScan_Generate_CWE_Report') {
				condition('SUCCESS')
				parameters {
					currentBuild()
				}
			}
		}
		downstreamParameterized {
    		    trigger('Send_Email') {
    			condition('FAILED')
    			parameters {
				predefinedProps([BUILDURL:'$BUILD_URL',JOBNAME:'$JOB_NAME',BUILDNUMBER:'$BUILD_NUMBER'])
    			}
    		}
    	}
	
	configure { project ->
		project / 'buildWrappers' / 'hudson.plugins.sonar.SonarBuildWrapper' {
			plugin('sonar@2.4.4')
		}
	}
}
	configure { project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' {
				postUrl('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1591')
			}
	}
    }

// ##### END OF SONAR JOB #####
// ##### GENERATE UpdateJiraCommentCIStatus JOB #####

generateUpdateJiraCommentCIStatus.with {
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
label('linux')
	steps{
		shell('''
if [ "$GITLAB_SOURCE_BRANCH" != "development" ]; then
  JIRA_TICKET=$(echo "$GITLAB_SOURCE_BRANCH" | sed "s/FEATURE-//g")
  echo "JIRA_TICKET=$JIRA_TICKET" > properties.txt
else
  echo "Not a feature branch, no action to take."
  exit 0
fi

echo "GITLAB_SOURCE_BRANCH=$GITLAB_SOURCE_BRANCH" >> properties.txt
			''')
		environmentVariables{
			propertiesFile('properties.txt')
		}
        conditionalSteps {
            condition {
                    not {
                        stringsMatch('$GITLAB_SOURCE_BRANCH', 'development', false)
                    }
                }
			 runner('Fail')
			 steps{}
        }
	}
	configure { project ->
		project / 'builders' / 'org.jenkinsci.plugins.conditionalbuildstep.ConditionalBuilder' / 'conditionalbuilders' / 'info.bluefloyd.jenkins.IssueUpdatesBuilder' {
			'restAPIUrl'('https://10.0.66.78:8443/jira/rest/api/2')
			'userName'('jenkins')
			'password'('scdsdevjenkinsadmin01')
			'jql'('project=$JIRA_PROJECT AND issuekey=$JIRA_TICKET')
			'workflowActionName'('')
			'comment'('$JIRA_COMMENT - $JENKINS_BUILD_URL')
			'customFieldId'('')
			'customFieldValue'('')
			'resettingFixedVersions'('false')
			'createNonExistingFixedVersions'('false')
			'failIfJqlFails'('false')
			'failIfNoIssuesReturned'('false')
			'failIfNoJiraConnection'('false')
        }
    }
    configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1592')
					}
			}
}

// ##### END OF UpdateJiraCommentCIStatus JOB #####
// ##### GENERATE Save_Latest_Development_CI JOB #####

generateSaveLatestDevelopmentCIJob.with{
logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
	label('linux2')
	steps{
		shell('''
echo "$LATEST_DEV_CI_URL" > LATEST_DEV_CI_URL.txt
		''')
	}

    configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1590')
					}
			}
}

// ##### END OF Save_Latest_Development_CI JOB #####
// ##### GENERATE Revert_Previous_Commit JOB #####

generateRevertPreviousCommitJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('linux2')
	scm {
			git {
					remote {
							url("http://10.0.66.104:9080/${PROJECT_NAME}.git")
							credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
					}
					branch('*/development')
			}
	}
	wrappers {
		environmentVariables {
			preBuildCleanup()
			envs([LATEST_WORKING_TAG:'Java_Latest_Working_Dev',PROJECT_NAME:workspacename+'/'+projectname])
		}
		credentialsBinding {
			usernamePassword('GITLAB_USERNAME','GITLAB_PASSWORD','05443b60-8117-43cb-9f49-34a50cc64a30')
		}
	}
	steps{
		shell('''
if [ -z $(git tag -l $LATEST_WORKING_TAG) ]
then
	echo "${LATEST_WORKING_TAG} tag not yet created so it cannot be reverted."
    exit 1
else
	git reset --hard $LATEST_WORKING_TAG TAG_HASH=$(git rev-list -n 1 ${LATEST_WORKING_TAG}) git commit -m "Reverting to the state of the project at $TAG_HASH." set +x git config remote.origin.url http://${GITLAB_USERNAME}:${GITLAB_PASSWORD}@10.0.66.104:9080/${PROJECT_NAME}.git git push -f origin HEAD:development fi''')	
	}
	publishers {
		downstreamParameterized {
				trigger('Update_Jira_Issue_In_Progress') {
							condition('SUCCESS')
							parameters {
								predefinedProps([ISSUE_KEY: '$ISSUE_KEY'])
							}
				}
		}
    }


    configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1588')
					}
			}
}

// ##### END OF Revert_Previous_Commit JOB #####
// ##### GENERATE Save_Deployment_Tag JOB #####

generateSaveTag.with {
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
	label('linux2')
	scm {
		git {
			remote {
				url("http://10.0.66.104:9080/${PROJECT_NAME}.git")
				credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
			}
			branch('*/development')
		}
	}
	wrappers {
		preBuildCleanup()
		environmentVariables {
			envs([LATEST_WORKING_TAG:'Java_Latest_Working_Dev',PROJECT_NAME:workspacename+'/'+projectname])
		}
		credentialsBinding {
			usernamePassword('GITLAB_USERNAME','GITLAB_PASSWORD','05443b60-8117-43cb-9f49-34a50cc64a30')
		}
	}
	steps {
		shell('''if [ -z $(git tag -l $LATEST_WORKING_TAG) ]
then
	git tag -a -m "Tag for latest working development." $LATEST_WORKING_TAG
else
    git tag -a -f -m "Tag for latest working development." $LATEST_WORKING_TAG
fi
set +x
git config remote.origin.url http://${GITLAB_USERNAME}:${GITLAB_PASSWORD}@10.0.66.104:9080/${PROJECT_NAME}.git
git push origin :refs/tags/${LATEST_WORKING_TAG}
git push --tags''')
	}
   configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1589')
					}
			}
}
// ##### END OF Save_Deployment_Tag JOB #####
// ##### GENERATE Update_Jira_Issue_In_Progress JOB #####

generateUpdateJiraIssueInProgressJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}
	label('linux2')
	customWorkspace('$CUSTOM_WORKSPACE')
	steps {
        progressJiraIssues {
            jqlSearch('issuekey=$ISSUE_KEY')
            workflowActionName('Merge CI Failed')
            comment('''
Jenkins Build Tag:
$BUILD_TAG

Updated to "Build In Progress"
			''')
        }
    }
   configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1595')
					}
			}
}

// ##### END OF Update_Jira_Issue_In_Progress JOB #####
// ##### GENERATE Update_Jira_Issue_Awaiting_Deployment JOB #####

generateUpdateJiraIssueAwaitingDeploymentJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
}
	label('linux2')
	customWorkspace('$CUSTOM_WORKSPACE')
	wrappers {
        timestamps()
    }
	steps {
        progressJiraIssues {
            jqlSearch('issuekey=$ISSUE_KEY')
            workflowActionName('Merge Completed')
            comment('''
Jenkins Build Tag:
$BUILD_TAG

Updated to "Awaiting DEV Deployment"

Jenkins Build - $BUILD_URL''')
        }
    }
	publishers {
			downstreamParameterized {
					trigger('Delete_Feature_Branch') {
								condition('SUCCESS')
								parameters {
									predefinedProps([BRANCH: '$FEATURE_BRANCH',GITLAB_SOURCE_REPOHOMEPAGE:'$GITLAB_SOURCE_REPOHOMEPAGE'])
									currentBuild()
								}
					}
			}
    }
   configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1593')
					}
			}
}


// ##### END OF Update_Jira_Issue_Awaiting_Deployment JOB #####
// ##### GENERATE CREATE FEATURE BRANCH JOB #####

generateCreateBranchJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('docker')
	configure { project ->
		project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' {
			'gitLabConnection'('ADOP-GitLab')
		}
	}
	parameters {
		stringParam('ISSUE_KEY')
	}
	wrappers {
		preBuildCleanup()
        timestamps()
    }
    configure { Node project ->
        project / 'buildWrappers' / 'EnvInjectPasswordWrapper' (plugin:'envinject@1.91.3') { 
             injectGlobalPasswords true 
             maskPasswordParameters true
             passwordEntries ('')
             }
}
	scm {
			git {
					remote {
							url("http://10.0.66.104:9080/${PROJECT_NAME}.git")
							credentials('05443b60-8117-43cb-9f49-34a50cc64a30')
					}
					branch('*/development')
			}
	}
	steps {
		environmentVariables {
			propertiesFile('/var/jenkins_home/cicd_config/Common/entrypoint.txt')
		}
		shell('''docker exec gitlab mv /var/opt/gitlab/git-data/repositories/'''+"${PROJECT_NAME}"+'''.git/custom_hooks/pre-receive /var/opt/gitlab/git-data/repositories/'''+"${PROJECT_NAME}"+'''.git/custom_hooks/branch-creation

renToPR="docker exec gitlab mv /var/opt/gitlab/git-data/repositories/'''+"${PROJECT_NAME}"+'''.git/custom_hooks/branch-creation /var/opt/gitlab/git-data/repositories/'''+"${PROJECT_NAME}"+'''.git/custom_hooks/pre-receive"
		
for var in $(git branch -r | sed 's;^.*/;;');do if [ "$var" = "FEATURE-$ISSUE_KEY" ];then echo "A branch named '$var' already exists.";$renToPR;exit;fi;done

set +x

GIT_REPO=$(echo "$GIT_URL" | cut -d'/' -f4,5)
GIT_PROJECT_REPO=$(echo "$GIT_REPO" | sed 's/.git//')
echo $GIT_REPO
# Create branch
project=$(echo $GIT_PROJECT_REPO | sed 's,/,%2F,')
token="$(curl -X POST "$GITLAB_URL/api/v3/session?login=$GITLAB_USERNAME&password=$GITLAB_PASSWORD" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['private_token'];")"
pid="$(curl --header "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$project" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['id'];")" 	
curl -X POST -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/repository/branches?branch_name=FEATURE-$ISSUE_KEY&ref=development"

echo "GIT_REPO_URL=$GITLAB_URL/$GIT_REPO" > properties.txt

#Rename Branch Creation hook to Pre-Receive
$renToPR''')
		environmentVariables {
		propertiesFile('properties.txt')
		}
	}
	configure { project ->
		project / 'builders' / 'info.bluefloyd.jenkins.IssueUpdatesBuilder' {
			'restAPIUrl'('https://10.0.66.78:8443/jira/rest/api/2')
			'userName'('jenkins')
			'password'('scdsdevjenkinsadmin01')
			'jql'('project=NTP AND issuekey=$ISSUE_KEY')
			'workflowActionName'('')
			'comment'('FEATURE-$ISSUE_KEY branch created in $GIT_REPO_URL')
			'customFieldId'('')
			'customFieldValue'('')
			'resettingFixedVersions'('false')
			'createNonExistingFixedVersions'('false')
			'failIfJqlFails'('false')
			'failIfNoIssuesReturned'('false')
			'failIfNoJiraConnection'('false')
		}
	}
   configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1585')
					}
			}
}
// ##### END OF CREATE BRANCH JOB #####
// ##### GENERATE DELETE FEATURE BRANCH JOB #####

generateDeleteBranchJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('linux2')
	customWorkspace('$CUSTOM_WORKSPACE')
	wrappers {
        timestamps()
    }
	steps{
		shell('''
set +x

project=$(echo $GITLAB_SOURCE_REPOHOMEPAGE | cut -d '/' -f 4-5 | sed 's/\\//\\%2F/')
token="$(curl -X POST "$GITLAB_URL/api/v3/session?login=$GITLAB_USERNAME&password=$GITLAB_PASSWORD" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['private_token'];")"
pid="$(curl --header "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$project" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['id'];")"

# Delete branch 
curl -X DELETE -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/repository/branches/${BRANCH}"		
		''')
	}

   configure { Node project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' (plugin:'jenkins-jira-plugin@2.1.0') {
            	postUrl ('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1586')
					}
			}
}


// ##### END OF DELETE BRANCH JOB #####
// ##### GENERATE Update_Jira_Issue_Code_Review JOB #####

generateUpdateJiraIssueCodeReviewJob.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
    
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('linux2')
	configure { project ->
			project / 'triggers' / 'com.dabsquared.gitlabjenkins.GitLabPushTrigger' {
				spec('')
				triggerOnPush('false')
				triggerOnMergeRequest('true')
				triggerOpenMergeRequestOnPush('never')
				ciSkip('true')
				setBuildDescription('true')
				addNoteOnMergeRequest('true')
				addCiMessage('false')
				addVoteOnMergeRequest('true')
				branchFilterType('All')
				includeBranchesSpec('')
				excludeBranchesSpec('')
				targetBranchRegex('')
				acceptMergeRequestOnSuccess('false')
			}
	}
	wrappers {
        timestamps()
    }
    configure { Node project ->
        project / 'buildWrappers' / 'EnvInjectPasswordWrapper' (plugin:'envinject@1.91.3') { 
             injectGlobalPasswords true 
             maskPasswordParameters true
             passwordEntries ('')
             }
}
	steps {
			environmentVariables{
				propertiesFile('${JENKINS_HOME}/cicd_config/Common/entrypoint.txt')
			}
			shell ('''
#!/bin/bash

set +x

project=$(echo $gitlabSourceRepoHomepage | cut -d '/' -f 4-5 | sed 's,/,%2F,')
project_name=$(echo $gitlabSourceRepoHomepage | cut -d '/' -f 4-5)
token="$(curl -X POST "$GITLAB_URL/api/v3/session?login=$GITLAB_USERNAME&password=$GITLAB_PASSWORD" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['private_token'];")"
pid="$(curl --header "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$project" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['id'];")"

AUTHOR_NAME="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['author']['name'];")"

if [ "$gitlabActionType" = "MERGE" ]
	then
	
	if [ $(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['assignee']['name'];") ]; then
		echo "SEND_EMAIL=0" > properties.txt
        
        echo "MR has an assignee"
		ASSIGNEE_NAME="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['assignee']['name'];")"
		echo "Assignee name: $ASSIGNEE_NAME"

		x="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['iid'];")"

		ISSUE_KEY="$(echo $gitlabSourceBranch | grep -Po NTP-[0-9]+)"
		echo "ISSUE_KEY=$ISSUE_KEY" >> properties.txt
                
        GITLAB_MERGE_REQUEST_URL=$(echo "$gitlabSourceRepoHomepage/merge_requests/$x")
        echo "GITLAB_MERGE_REQUEST_URL=$GITLAB_MERGE_REQUEST_URL" >> properties.txt
        
		curl -k -D- -u $JIRA_USERNAME:$JIRA_PASSWORD -X PUT --data "{\"name\":\"$ASSIGNEE_NAME\"}" -H "Content-Type: application/json" $JIRA_URL/rest/api/2/issue/$ISSUE_KEY/assignee
	else
		AUTHOR_USERNAME="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['author']['username'];")"
		AUTHOR_ID="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/users?username=$AUTHOR_USERNAME" | python -c "import json,sys;obj=json.load(sys.stdin);print obj[0]['id'];")"
		MR_IID="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['iid'];")"
		AUTHOR_EMAIL="$(curl -X GET -H "PRIVATE-TOKEN: $token" "$GITLAB_URL/api/v3/users/$AUTHOR_ID" | python -c "import json,sys;obj=json.load(sys.stdin);print obj['email'];")"
		echo "Merge Request has no assignee. Sending an e-mail to $AUTHOR_NAME - $AUTHOR_EMAIL"
		echo "Closing Merge Request $MR_IID"
		curl --header "PRIVATE-TOKEN: $token" -X PUT "$GITLAB_URL/api/v3/projects/$pid/merge_requests/$gitlabMergeRequestId?state_event=close"
		echo "AUTHOR_EMAIL=$AUTHOR_EMAIL" > properties.txt
		echo "MR_IID=$MR_IID" >> properties.txt
		echo "PROJECT=$project_name" >> properties.txt
        echo "SEND_EMAIL=1" >> properties.txt
	fi
fi
			''')
			environmentVariables {
				propertiesFile('properties.txt')
			}
			progressJiraIssues {
				jqlSearch('issuekey=$ISSUE_KEY')
				workflowActionName('Request Code Review')
				comment('''
Jenkins Build Tag:
$BUILD_TAG

URL to GitLab Merge Request:
$GITLAB_MERGE_REQUEST_URL

Updated to "Pending Code Review"
			''')
        }
    }
	publishers{
		flexiblePublish {
			conditionalAction {
                condition {
                        stringsMatch('$SEND_EMAIL', '1', false)
                }
				publishers {
					steps{
                        shell('exit 1')
                    }
				}
			}
		}
		extendedEmail {
			recipientList('$AUTHOR_EMAIL')
			defaultSubject('Your Merge Request has no assignee')
			defaultContent('''Please include an Assignee to your Merge Request.

			Closing your current MR: $GITLAB_URL/$PROJECT/merge_requests/$MR_IID''')
			contentType('text/html')
			triggers {
				failure {
					sendTo {
						recipientList()
					}
				}
			}
		}
	}
	configure { project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' {
				postUrl('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/1594')
			}
	}
}

// ##### END OF Update_Jira_Issue_Code_Review JOB #####
// ##### GENERATE Send_Email JOB #####

generateSendEmail.with{
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('linux2')
	parameters {
			stringParam('BUILDURL', '', '')
			stringParam('JOBNAME', '', '')
			stringParam('BUILDNUMBER', '', '')
	}
	publishers {
        extendedEmail {
            recipientList('$DEFAULT_RECIPIENTS')
			replyToList('$DEFAULT_REPLYTO')
            defaultSubject('$JOBNAME- Build # $BUILDNUMBER - Failed!')
            defaultContent('''$JOBNAME- Build # $BUILDNUMBER - Failed:

Check console output at ${BUILDURL}/console to view the results.''')
            contentType('default')
			preSendScript('$DEFAULT_PRESEND_SCRIPT')
            triggers {
                always {
					sendTo {
						recipientList()
					}
				}
            }
        }
    }
	configure { project ->
			project / 'publishers' / 'org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier' {
				postUrl('https://10.0.66.78:8443/jira/rest/jenkins/1.0/build/2453')
			}
	}
    }
// ##### END Send_Email JOB #####
// ##### CI Upload To Nexus #####
generateCiUploadNexus.with {
job('ci') {
	logRotator {
		daysToKeep(7)
    	artifactDaysToKeep(7)
		artifactNumToKeep(10)
    }
configure { project ->
            project / 'properties' / 'hudson.plugins.buildblocker.BuildBlockerProperty' (plugin:'build-blocker-plugin@1.7.3') {
                useBuildBlocker(false)
                    blockLevel('GLOBAL')
                	scanQueueFor('DISABLED')
                	 blockingJob ('')
            				}
        			}
configure { Node project ->
            project / 'properties' / 'com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty' (plugin:'gitlab-plugin@1.2.3') {
                gitLabConnection 'ADOP-GitLab'
           			 }
      		  }
configure { project ->
		project / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' (plugin:'rebuild@1.25') {
			autoRebuild false
            rebuildDisabled false
		}
	}

	label('linux2')
    customWorkspace('$CUSTOM_WORKSPACE_DEPLOY')
	wrappers {
	timestamps()
 		preBuildCleanup()
 		environmentVariables {
 			envs([GIT_PROJECT_NAME:projectname,GIT_TAG_PREFIX:workspacename+'_'+projectname+'_'+'DEV'])
 		}
	}
configure { Node project ->
        project / 'buildWrappers' / 'org.jenkinsci.plugins.credentialsbinding.impl.SecretBuildWrapper' (plugin:'credentials-binding@1.6') / 'bindings' / 'org.jenkinsci.plugins.credentialsbinding.impl.UsernamePasswordMultiBinding' {
            credentialsId '5bfa2df1-2413-4e17-ad0e-a4dc618de34d'
			usernameVariable 'NEXUS_USERNAME'
			passwordVariable 'NEXUS_PASSWORD'
 		      	 }
        	}
    
steps {
		copyArtifacts('Build_Java_Project') {
            includePatterns('**/*')
            buildSelector {
                upstreamBuild(false)
            }
        }
    shell('''#!/bin/bash
if [[ $gitlabSourceBranch == "development" ]]; then
  latestDir="${GIT_TAG_PREFIX}_Latest"
  
  set +x
  if [ -d $latestDir ]; then
      rm -rf $latestDir
  fi
  
  mkdir $latestDir
  
for MODULE in $(grep '<module>' pom.xml | sed -e 's/^[ \\t]*<module>\\(.*\\)<\\/module>/\\1/' | tr -d '\\r')
 do
 	echo "Module: $MODULE"
 	cd $MODULE/target
     
     find . -maxdepth 1 -name "*.jar" -o -name "*.war" -type f | sed 's,^./,,' | sort > artifactList.txt
     
     cd -
 	#for PACKAGE in $(find . -maxdepth 1 -name "*.jar" -o -name "*.war" -type f | sed 's,^./,,')
     while read line
     do
         #PACKAGE=$MODULE/target/$PACKAGE_FILE
         PACKAGE=$MODULE/target/$line
         echo "Package: $PACKAGE"
     
     if [[ $PACKAGE == *.war ]]; then \\
 		echo package $PACKAGE is a war file
 #Deploy to Weblogic and copy to package dir
 		filename=${PACKAGE##*/}
 		echo $filename
 		cp -p ${PACKAGE} ${PACKAGE_DIR}
 		cksum ${PACKAGE} | cut -d' ' -f1 > ${PACKAGE_DIR}/${filename}.cksum
 	fi
 	if [[ $PACKAGE == *.jar ]]; then \\
 #Upload artifact to inhouse nexus repo
 		echo package $PACKAGE is a jar file
 
 /var/jenkins_home/tools/hudson.tasks.Maven_MavenInstallation/ADOP_Maven/bin/mvn deploy:deploy-file \\
 -DrepositoryId=$NEXUS_USERNAME  \\
 -Durl=http://10.0.66.104/nexus/content/repositories/inhouse \\
 -DpomFile=$MODULE/pom.xml \\
 -Dfile=$PACKAGE \\
 -DgeneratePom=false
 
 		mvn_status=`echo $?`
         echo "maven status" $mvn_status
         if [[ $mvn_status -ne 0 ]]; then  \\
         	exit 1
         fi
 		
 #Copy to package dir
 		filename=${PACKAGE##*/}
 		echo $filename
 		cp -p ${PACKAGE} ${PACKAGE_DIR}
 		cksum ${PACKAGE} | cut -d' ' -f1 > ${PACKAGE_DIR}/${filename}.cksum
 		
     fi
     done<$MODULE/target/artifactList.txt
 done
  
  #Upload to snapshots nexus repo
  cd $latestDir
  
  echo CI_BUILD_URL=$CI_BUILD_URL > buildURL.txt
  
  for i in $(ls)
  do
      curl -v -u $NEXUS_USERNAME:$NEXUS_PASSWORD --upload-file $i \\
      http://10.0.66.104/nexus/content/repositories/snapshots/Java/${GIT_PROJECT_NAME}/${latestDir}/	
  done
  
  
  cd ..

fi''')
}
    }
// ##### END CI Upload To Nexus #####